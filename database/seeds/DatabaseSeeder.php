<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        /*$this->call('UsersTableSeeder');
        $this->command->info('User table seeded!');*/

        $path = 'app/dev_docs/user.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Users table seeded!');

        $path = 'app/dev_docs/batiments.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Batiments table seeded!');
        
        $path = 'app/dev_docs/cp.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('CP table seeded!');
        
        $path = 'app/dev_docs/langues.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Langues table seeded!');
        
        $path = 'app/dev_docs/proprietaires.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Proprietaires table seeded!');
        
        $path = 'app/dev_docs/proprietaires_bat.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Proprietaire Batiments join table seeded!');

    }
}
