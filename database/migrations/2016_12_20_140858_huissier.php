<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Huissier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('huissiers', function (Blueprint $table) {
            $table->increments('ID_huissier');
            $table->string('nom');
            $table->string('adresse');
            $table->string('email');
            $table->string('telephone');
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('huissiers');
    }
}
