<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampagneBat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campagne_bat', function (Blueprint $table) {
            $table->integer('campagne_ID')->unsigned()->index();
            $table->foreign('campagne_ID')->references('ID_campagne')->on('campagne')->onDelete('restrict');
            $table->integer('batiment_ID')->unsigned()->index();
            $table->foreign('batiment_ID')->references('ID_batiment')->on('batiments')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagne_bat');
    }
}
