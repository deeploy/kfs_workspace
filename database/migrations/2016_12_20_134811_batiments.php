<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Batiments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batiments', function (Blueprint $table) {
            $table->increments('ID_batiment');
            $table->string('adresse');
            $table->string('num');
            $table->integer('superficie');
            $table->string('etage');
            $table->string('section_cad');
            $table->string('num_cad');
            $table->integer('type_bat_ID')->unsigned()->index();
            $table->foreign('type_bat_ID')->references('ID_type_bat')->on('type_bat')->onDelete('cascade');
            $table->integer('CP_ID')->unsigned()->index();
            $table->foreign('CP_ID')->references('ID_CP')->on('cp')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('batiments');
    }
}
