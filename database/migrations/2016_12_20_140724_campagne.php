<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Campagne extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campagne', function (Blueprint $table) {
            $table->increments('ID_campagne');
            $table->string('nom');
            $table->timestamp('created_at');
            $table->string('description');
            $table->string('status_active');
            $table->integer('user_ID')->unsigned()->index();
            $table->foreign('user_ID')->references('ID_user')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagne');
    }
}
