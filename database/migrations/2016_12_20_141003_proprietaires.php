<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Proprietaires extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietaires', function (Blueprint $table) {
            $table->increments('ID_proprietaire');
            $table->string('nom');
            $table->string('adresse');
            $table->integer('num');
            $table->string('zip');
            $table->string('telephone');
            $table->string('email');
            $table->string('ddn');
            $table->string('status_civility_ID')->unsigned()->index();
            $table->foreign('status_civility_ID')->references('ID_status_civility')->on('status_civility')->onDelete('restrict');
            $table->string('langue_ID')->unsigned()->index();
            $table->foreign('langue_ID')->references('ID_langue')->on('langues')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proprietaires');
    }
}
