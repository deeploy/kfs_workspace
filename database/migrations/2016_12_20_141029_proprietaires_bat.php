<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProprietairesBat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietaires_bat', function (Blueprint $table) {
            $table->integer('proprietaire_ID')->unsigned()->index();
            $table->foreign('proprietaire_ID')->references('ID_proprietaire')->on('proprietaires')->onDelete('restrict');
            $table->integer('batiment_ID')->unsigned()->index();
            $table->foreign('batiment_ID')->references('ID_batiment')->on('batiments')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proprietaires_bat');
    }
}
