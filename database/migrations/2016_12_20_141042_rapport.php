<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rapport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rapports', function (Blueprint $table) {
            $table->increments('ID_rapport');
            $table->string('adresse');
            $table->string('nom');
            $table->string('telephone');
            $table->string('rating');
            $table->boolean('copropriete');
            $table->timestamp('created_at');
            $table->string('description');
            $table->integer('user_ID')->unsigned()->index();
            $table->foreign('user_ID')->references('ID_user')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rapports');
    }
}
