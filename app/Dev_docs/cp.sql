-- MySQL dump 10.15  Distrib 10.0.28-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	10.0.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cp`
--

DROP TABLE IF EXISTS `cp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp` (
  `ID_CP` int(11) NOT NULL AUTO_INCREMENT,
  `CP` smallint(6) DEFAULT NULL,
  `commune` varchar(45) DEFAULT NULL,
  `division` int(2) DEFAULT NULL,
  PRIMARY KEY (`ID_CP`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp`
--

LOCK TABLES `cp` WRITE;
/*!40000 ALTER TABLE `cp` DISABLE KEYS */;
INSERT INTO `cp` VALUES (1,1070,'Anderlecht',1),(2,1160,'Auderghem',1),(3,1082,'Berchem-sainte-agathe',1),(4,1000,'Bruxelles',1),(5,1040,'Etterbeek',1),(6,1140,'Evere',1),(7,1190,'Forest',1),(8,1083,'Ganshoren',1),(9,1050,'Ixelles',1),(10,1090,'Jette',1),(11,1081,'Koekelberg',1),(12,1080,'Molenbeek-saint-jean',1),(13,1060,'Saint-gilles',1),(14,1210,'Saint-josse-ten-noode',1),(15,1030,'Schaerbeek',1),(16,1180,'Uccle',1),(17,1170,'Watermael-boitsfort',1),(18,1200,'Woluwe-saint-lambert',1),(19,1150,'Woluwe-saint-pierre',1),(20,1070,'Anderlecht',2),(21,1070,'Anderlecht',4),(22,1070,'Anderlecht',5),(23,1070,'Anderlecht',6),(24,1070,'Anderlecht',7),(25,1070,'Anderlecht',8),(26,1160,'Auderghem',2),(27,1082,'Berchem-sainte-agathe',2),(28,1040,'Etterbeek',2),(29,1040,'Etterbeek',3),(30,1040,'Etterbeek',4),(31,1140,'Evere',2),(32,1190,'Forest',2),(33,1190,'Forest',3),(34,1190,'Forest',4),(35,1083,'Ganshoren',2),(36,1050,'Ixelles',2),(37,1050,'Ixelles',3),(38,1050,'Ixelles',4),(39,1050,'Ixelles',5),(40,1050,'Ixelles',6),(41,1050,'Ixelles',7),(42,1090,'Jette',3),(43,1090,'Jette',4),(44,1081,'Koekelberg',2),(45,1080,'Molenbeek-saint-jean',2),(46,1080,'Molenbeek-saint-jean',3),(47,1080,'Molenbeek-saint-jean',4),(48,1080,'Molenbeek-saint-jean',6),(49,1060,'Saint-gilles',2),(50,1210,'Saint-josse-ten-noode',2),(51,1180,'Uccle',2),(52,1180,'Uccle',4),(53,1180,'Uccle',6),(54,1180,'Uccle',8),(55,1170,'Watermael-boitsfort',2),(56,1200,'Woluwe-saint-lambert',2),(57,1200,'Woluwe-saint-lambert',3),(58,1200,'Woluwe-saint-lambert',4),(59,1150,'Woluwe-saint-pierre',2),(60,1150,'Woluwe-saint-pierre',3),(61,1150,'Woluwe-saint-pierre',4),(62,1000,'Bruxelles',2),(63,1000,'Bruxelles',3),(64,1000,'Bruxelles',4),(65,1000,'Bruxelles',5),(66,1000,'Bruxelles',6),(67,1000,'Bruxelles',7),(68,1000,'Bruxelles',8),(69,1000,'Bruxelles',9),(70,1000,'Bruxelles',10),(71,1000,'Bruxelles',11),(72,1000,'Bruxelles',12),(73,1000,'Bruxelles',13),(74,1000,'Bruxelles',14),(75,1000,'Bruxelles',15),(76,1000,'Bruxelles',16),(77,1000,'Bruxelles',18),(78,1000,'Bruxelles',19),(79,1000,'Bruxelles',21),(80,1000,'Bruxelles',22),(81,1030,'Schaerbeek',2),(82,1030,'Schaerbeek',3),(83,1030,'Schaerbeek',4),(84,1030,'Schaerbeek',5),(85,1030,'Schaerbeek',6),(86,1030,'Schaerbeek',8),(87,1030,'Schaerbeek',9),(88,1030,'Schaerbeek',10),(89,1030,'Schaerbeek',11);
/*!40000 ALTER TABLE `cp` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-23 11:18:14
