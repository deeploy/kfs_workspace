-- MySQL dump 10.15  Distrib 10.0.28-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	10.0.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `type_bat`
--

DROP TABLE IF EXISTS `type_bat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_bat` (
  `ID_type_bat` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_type_bat` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID_type_bat`)
) ENGINE=InnoDB AUTO_INCREMENT=297 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_bat`
--

LOCK TABLES `type_bat` WRITE;
/*!40000 ALTER TABLE `type_bat` DISABLE KEYS */;
INSERT INTO `type_bat` VALUES (1,'Abattoir'),(2,'Abri'),(3,'Abri trans'),(4,'Admin.geb.'),(5,'Afdak'),(6,'Afvalverw.'),(7,'Appart'),(8,'At.constr.'),(9,'At.protege'),(10,'Atelier'),(11,'B.aide soc'),(12,'B.animaux'),(13,'B.parcage'),(14,'B.scolaire'),(15,'B.telecom.'),(16,'Badinricht'),(17,'Bakkerij'),(18,'Bank'),(19,'Banque'),(20,'Baraquem.'),(21,'Bassin gew'),(22,'Bassin ord'),(23,'Bat.admin.'),(24,'Bat.bureau'),(25,'Bat.culte'),(26,'Bat.funer.'),(27,'Bat.gouv.'),(28,'Bat.histor'),(29,'Bat.hospit'),(30,'Bat.indust'),(31,'Bat.justic'),(32,'Bat.milit.'),(33,'Bat.penit.'),(34,'Bat.rural'),(35,'Beb.opp.a'),(36,'Beb.opp.g'),(37,'Beb.opp.n'),(38,'Beb.opp.u'),(39,'Bedrijfsc#'),(40,'Bergplaats'),(41,'Bescher/w.'),(42,'Biblioth.'),(43,'Bioscoop'),(44,'Bois'),(45,'Boomg.hoog'),(46,'Boomkwek.'),(47,'Bos'),(48,'Boulanger.'),(49,'Bourse'),(50,'Bouwgrond'),(51,'Bouwland'),(52,'Bouwmat/f.'),(53,'Brasserie'),(54,'Brouwerij'),(55,'Building'),(56,'C.recher.'),(57,'Cab.elect.'),(58,'Cab.gaz'),(59,'Cab.tel.'),(60,'Cabine'),(61,'Cafe'),(62,'Canal'),(63,'Captag.eau'),(64,'Carriere'),(65,'Casino'),(66,'Cementfab.'),(67,'Centr.cult'),(68,'Centr.elec'),(69,'Chantier'),(70,'Chapelle'),(71,'Charcuter.'),(72,'Chateau'),(73,'Chemic/fab'),(74,'Chemin'),(75,'Chemin fer'),(76,'Cimetiere'),(77,'Cinema'),(78,'Commerce'),(79,'Constr/wpl'),(80,'Cour'),(81,'Couvent'),(82,'Creche'),(83,'D.parking#'),(84,'Dijk'),(85,'Drankhuis'),(86,'Drukkerij'),(87,'Duiventil'),(88,'Ecurie'),(89,'Eglise'),(90,'Elek.cabin'),(91,'Elek.centr'),(92,'Elek.mat.f'),(93,'Entrepot'),(94,'Etab.bains'),(95,'Etab.cure'),(96,'Etang'),(97,'Expl.ind.#'),(98,'F.a.usuels'),(99,'F.alim.bet'),(100,'F.art.cuir'),(101,'F.boissons'),(102,'F.couleurs'),(103,'F.habillem'),(104,'F.jouets'),(105,'F.mat.cons'),(106,'F.mat.elec'),(107,'F.meubles'),(108,'F.plastiq.'),(109,'F.pr.alim.'),(110,'Feestzaal'),(111,'Ferme'),(112,'Flat'),(113,'Forge'),(114,'Fosse'),(115,'G.d.ap.geb'),(116,'Gar.atel.'),(117,'Gar.depot'),(118,'Gar.stelpl'),(119,'Gar.werkpl'),(120,'Garage'),(121,'Gascabine'),(122,'Gazometer'),(123,'Gazometre'),(124,'Gd.magasin'),(125,'Geb.ered.'),(126,'Gem/huis'),(127,'Gendarmer.'),(128,'Gerechtsh.'),(129,'Gezantsch.'),(130,'Gouver/geb'),(131,'Gr.warenh.'),(132,'Gracht'),(133,'Groeve'),(134,'Grond'),(135,'Hab.vacan.'),(136,'Hand/huis'),(137,'Hangar'),(138,'Hoeve'),(139,'Hooiland'),(140,'Hotel'),(141,'Huis'),(142,'Huis#'),(143,'Imprimerie'),(144,'Ins.epurat'),(145,'Inst.frigo'),(146,'Inst.sport'),(147,'Jardin'),(148,'Jeugdheem'),(149,'K.paleis'),(150,'K.veeteelt'),(151,'Kaai'),(152,'Kanaal'),(153,'Kantoorgeb'),(154,'Kapel'),(155,'Kasteel'),(156,'Kerk'),(157,'Kerkhof'),(158,'Kinderbew.'),(159,'Kiosk'),(160,'Kiosque'),(161,'Klooster'),(162,'Koelinr.'),(163,'Koer'),(164,'Krotwoning'),(165,'Kult.centr'),(166,'Landgebouw'),(167,'Lavatory'),(168,'Lavoir'),(169,'Legation'),(170,'Lijkenhuis'),(171,'M.commun.'),(172,'M.d.ap.geb'),(173,'Magazijn'),(174,'Mais.jeun.'),(175,'Mais.repos'),(176,'Maison'),(177,'Maison#'),(178,'Marais'),(179,'March.couv'),(180,'Mare'),(181,'Menuiserie'),(182,'Metallurg.'),(183,'Meubelfab.'),(184,'Meunerie'),(185,'Milit.geb.'),(186,'Moeras'),(187,'Monument'),(188,'Moskee'),(189,'Mosquee'),(190,'Musee'),(191,'Museum'),(192,'Nijv/geb.'),(193,'Nijv/grond'),(194,'Ondergr.r.'),(195,'Onderzoekc'),(196,'Opp.& g.d.'),(197,'Orphelinat'),(198,'Over.markt'),(199,'P.p.im.ap.'),(200,'P.parc st#'),(201,'Paardestal'),(202,'Pal.royal'),(203,'Papeterie'),(204,'Papierfab.'),(205,'Parc'),(206,'Park'),(207,'Parkeergeb'),(208,'Parking'),(209,'Part.comm.'),(210,'Pastorie'),(211,'Pature'),(212,'Paviljoen'),(213,'Pavillon'),(214,'Pepiniere'),(215,'Pigeonnier'),(216,'Piscine'),(217,'Pl.jeux'),(218,'Place'),(219,'Plein'),(220,'Point vue'),(221,'Pre'),(222,'Presbytere'),(223,'Pt.elevage'),(224,'Puin'),(225,'Remise'),(226,'Reservoir'),(227,'Restaurant'),(228,'Ruines'),(229,'Rusthuis'),(230,'S.exposit.'),(231,'S.fetes'),(232,'S.spectacl'),(233,'Schoolgeb.'),(234,'Schrijnw.'),(235,'Scierie'),(236,'Serre'),(237,'Serv.stat.'),(238,'Slachterij'),(239,'Souterrain'),(240,'Speelterr.'),(241,'Spekt/zaal'),(242,'Spoorweg'),(243,'Sportgeb.'),(244,'Sportterr.'),(245,'Stat.serv.'),(246,'Station'),(247,'Stort.exp.'),(248,'Sup.& p.c.'),(249,'Sup.bat.a'),(250,'Sup.bat.e'),(251,'Sup.bat.i'),(252,'Sup.bat.o'),(253,'Synagoge'),(254,'Synagogue'),(255,'Taudis'),(256,'Telecom/g.'),(257,'Tempel'),(258,'Temple'),(259,'Terr.batir'),(260,'Terr.camp.'),(261,'Terr.indus'),(262,'Terr.marai'),(263,'Terr.sport'),(264,'Terrain'),(265,'Terre'),(266,'Terre v.v.'),(267,'Textielfab'),(268,'Theater'),(269,'Theatre'),(270,'Toonzaal'),(271,'Tr.immondi'),(272,'Tuin'),(273,'U.chimique'),(274,'Uitkijk'),(275,'Universit.'),(276,'Universite'),(277,'Usine gaz'),(278,'Verger b.t'),(279,'Verger h.t'),(280,'Verpl/inr.'),(281,'Verrerie'),(282,'Vijver'),(283,'Vleesw/fab'),(284,'Voedings/f'),(285,'Wachthuis'),(286,'Warmoesgr.'),(287,'Wasserij'),(288,'Watertoren'),(289,'Waterwinn.'),(290,'Weg'),(291,'Weiland'),(292,'Welzijnsg.'),(293,'Werkplaats'),(294,'Windmolen'),(295,'Woeste gr.'),(296,'Zagerij');
/*!40000 ALTER TABLE `type_bat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-23 11:19:34
