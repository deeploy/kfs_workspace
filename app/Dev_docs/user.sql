-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Client: localhost:3306
-- Généré le: Ven 23 Décembre 2016 à 09:42
-- Version du serveur: 10.0.28-MariaDB
-- Version de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `deeploy_kfs_2`
--

--
-- Contenu de la table `user`
--

INSERT INTO `users` (`nom`, `prenom`, `email`, `telephone`,`pass`, `users_status_ID`) VALUES
('Assent', 'Kevin', 'kevin@eazy-web.be','0471689397', 'ce41c341508db96a5be2da513b6e58e264861951', 0),
('Akabi', 'Omar', 'omarakabi@yahoo.fr','', 'a9e2cfcc2debda61028fe298eaf2ad45f7d51886', 0),
('Vandevorst', 'Katty', 'kv@biblimmo.be','', 'a9e2cfcc2debda61028fe298eaf2ad45f7d51886', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
