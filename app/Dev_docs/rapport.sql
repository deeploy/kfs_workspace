-- MySQL dump 10.15  Distrib 10.0.28-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	10.0.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `ID_report` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `copro` tinyint(1) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `description` text NOT NULL,
  `user_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID_report`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` VALUES (6,'Rue Antoine BrÃ©art 55','M\'RAD KAOUACH, MARC','',3,0,1464347803,'Monsieur M\'rad dÃ©sire vendre pour ne plus s\'occuper de ses locataires.\r\nIl ne dÃ©sire pas donner son numÃ©ro de tÃ©lÃ©phone pour Ãªtre contactÃ©.\r\nIl part du 3 juin 2016 pendant 3 mois et appellera probablement Ã  son retour ou juste avant de partir.',11),(15,'rue de thy 91','smeeters marc','02/731.59.04',4,0,1464621074,'Faire une proposition de date pour une visite.\r\nMaison unifamiliale',11),(16,'R GALLAIT 147','MUTLU, YUVANA','0472/28.06.06',5,0,1464702018,'3 Ã©tages + 1 rdc commercial.\r\nLoyer :\r\n1500 euros rdc commercial\r\n700 euros 1er Ã©tage\r\n650 euros 2Ã¨me Ã©tage\r\n670 euros 3Ã¨me Ã©tage\r\n\r\nTous les baux sont enregistrÃ©s\r\n\r\nPS : Il avait donnÃ© un autre numÃ©ro Ã  Katty mais ne veut pas qu\'on le dÃ©range sur ce numÃ©ro lÃ  (0488/93.99.97)\r\n',11),(17,'R CRICKX 10','STAVRIDIS, KONSTANTINOS','0479/33.31.89',0,0,1464624511,'Ne sais pas si est intÃ©ressÃ© Ã  vendre\r\n4 Ã©tages (2 piÃ¨ces par Ã©tage)\r\n1 cave',11),(18,'AV DU PARC 28','GODTS, PHILIPPE PAUL & HANSSENS, MURIEL LOUISE ','0485/70.17.13',3,0,1465568821,'Aucune information complÃ©mentaire',11),(19,'KONINGSSTR 284','theo vanuytrecht (firme Waalter)','0477/385.638',5,0,1465568844,'Contacter Waalter pour la vente',0),(20,'R ROYALE 275','VERCRUYSSE, BERNARD SIMON','0478/48.99.27',4,0,1465569813,'Un appartement Duplex au 4Ã¨me Ã©tage\r\n3 chambres\r\nPossibilitÃ© de garage\r\n1 cave',11),(21,'R LINCOLN 71','CHARLIER, DANIELLE MARGUERITE','0477/41.97.84',4,0,1465803414,'BÃ¢timent de 1906.\r\nBÃ¢timent officiellement en unifamiliale (mais mis en tant qu\'appartemment).\r\nMme Charlier a l\'usufruit de la propriÃ©tÃ©, mais ce sont ces 2 enfants qui sont propriÃ©taires.\r\nDÃ©jÃ  expertisÃ© par Trevi.\r\n',11),(22,'AV ERNEST RENAN 15','SOCIETE/VINCENT SMEULDERS','02/308.05.30',3,0,1465808181,'Maison de MaÃ®tre de 1905. RÃ©novation en 2009.\r\nCabinet au rez-de-chaussÃ©e + dulex au dessus.\r\nMaison unifamiliale.\r\nGarage + jardin.\r\nJoignable jusque DEMAIN SOIR LIMITE.\r\nPas de prÃ©fÃ©rence pour l\'heure.\r\nD\'aprÃ¨s ce qu\'il dit, il a dÃ©jÃ  reÃ§u d\'autres lettres type comme les nÃ´tres.',11),(23,'R ROYALE 205','KIR, EMEL ','0474/57.51.38',3,1,1465811743,'Demande d\'info. PossibilitÃ© de vendre, ca dÃ©pend du prix nÃ©gociÃ©.\r\nImmeuble de rapport.\r\nRez-de-chaussÃ©e et premier Ã©tage appartenant Ã  madame Kir Emel.\r\nDeuxiÃ¨me et troisiÃ¨me Ã©tage appartenant au frÃ¨re de madame Kir Emel.\r\nLes deux seraient intÃ©ressÃ©s de vendre.\r\nRez-de-chaussÃ©e est un bureau (louÃ© par une personne de la commune apparemment (mais pas sÃ»r)).',11),(24,'R ROYALE 239','verburgh patricia','0478/933113',0,1,1465829668,'Studio Ã©tudiant\r\nAppartemment',11),(25,'R ROYALE 57','Pas de nom','084/93.64.04',0,0,1465829771,'bÃ¢timent inconnu dans le cadastre .. ??\r\n2 familles qui l\'occupe. Maison de maÃ®tre',11),(26,'R ROYALE 23','VAN ROY, GUY PAUL','0496/449.252',4,0,1465830155,'Droit de prÃ©fÃ©rence du bÃ¢timent situÃ© Ã  l\'arriÃ¨re qui en demande urbanistique.\r\n3 Ã©tages + rez de chaussÃ©e.',11),(27,'R ROYALE 219','M. Pasque','0488/981.033',5,0,1465830832,'Immeuble comportant : \r\nRez de chaussÃ©e commercial\r\n1er et 2Ã¨me Ã©tage : duplex occupÃ© par le propriÃ©taire\r\n3Ã¨me : 4 studios (700â‚¬ chacun)\r\nIl aimerait vendre pour 2000â‚¬/mÂ²\r\nPersonne de contact : M.Pasque => Mari de la propriÃ©taire BIZIEN, DELPHINE MARIE',11),(28,'KONINGSSTR 168','Mme. Cekik','02/221.04.82',5,0,1465830782,'Appartient Ã  la banque VENNOOTSCHAP/DEMIR-HALK BANK NEDERLAND TE ROTTERDAM, et la banque est locataire.\r\nRez-de-chaussÃ©e, 1er, 2Ã¨me, 3Ã¨me : Bureau\r\n4Ã¨me : rÃ©sidence\r\nTÃ©lÃ©phoner entre 9.00 et 17.00',11),(29,'R ROYALE 314','ZONE, RAYMOND MARIE & KOLEVA, NEDKA SIMEONOVA ','02/640.15.86',5,0,1465919486,'rez-de-chaussÃ©e + 3 Ã©tages\r\nValeur locative :\r\n6 chambres : 400 euros\r\npetite maison arriÃ¨re : 400 euros\r\ngarage : 120 euros\r\n\r\nPossibilitÃ© de faire un rdc bureau ou autre (les propriÃ©taires ont l\'air de s\'en foutre).\r\nLe tÃ©lÃ©phone n\'a pas beaucoup de sonneries, donc il faudra insister car la maison est grande.\r\nElle loue la petite maison arriÃ¨re (donc ca lui appartiendrait apparemment). Ce qui fait qu\'il y a deux accÃ¨s Ã  la maison : de la rue de la poste, et de la rue Royale.\r\n',11),(30,'R ROYALE 251, R ROYALE 268, R ROYALE 272','M. Greant','0477/133.033',5,0,1465984917,'Maison unifamiliale exploitÃ©e en 42 studios Ã©tudiant. Reconnu par BRYC (http://www.bryc.be/ecole/index.php), il travaille avec cette Ã©cole.\r\n+ Rapport company web',11),(31,'R ROYALE 330','DAMBROT, JEANETTE','024799501',0,0,1465991543,'BÃ¢timent dÃ©jÃ  vendu il y a un an.\r\nNouveau propriÃ©taire inconnu\r\nrez commercial',11),(32,'R MARCONI 179','STEENBERGEN, MYRIAM FERNANDE','',2,0,1466429664,'Nouvelle adresse :\r\nDikkemeerweg 126\r\n1653 Dworp\r\n',0),(33,'R ROYALE 23','VAN ROY, GUY PAUL','016/47.79.74',5,1,1466503302,'TrÃ¨s intÃ©ressÃ© Ã  vendre, mais la maison part en indivision Ã  ses enfants',0),(34,'AV BEL AIR 39','MATHY, FREDERIC BERNARD','0477/38.38.49',5,0,1466503674,'Maison unifamiliale des annÃ©es 40. Vide, sans locataire.\r\n3 niveaux + rez + comble.\r\n11 mÃ¨tres de facade\r\n11 mÃ¨tres de profondeur\r\n177 mÂ² au sol\r\nComporte une annexe cuisine, salle Ã  manger\r\nLe numÃ©ro de contact est celui de l\'architecte du bÃ¢timent (TriviÃ¨re Denis), qui reprÃ©sente M.Mathy (le propriÃ©taire).\r\nPas d\'estimation du prix de vente.\r\n',0),(35,'R ROYALE 263','POPESCU, ANETA','0477/63.62.88',5,1,1466505588,'Immeuble 8 appartemments oÃ¹ 4 propriÃ©taires dÃ©sirent vendre (Mme.Popescu : 3Ã¨me ; M.Pugh : 2Ã¨me ; M.Kains : 1er ; M. Donchenco : rez).\r\n5 Ã¨me personne, Pearce, Alan 1er Ã©tage avant va bientÃ´t partir Ã  la retraite, et donc va vendre son appartement.\r\nPour l\'instant ils sont en infraction urbanistique. Dossier en cours de jugement Ã  la commission de concertation, introduit au mois de mars cette annÃ©e.',0),(36,'MATTHYSSTR 44','DEPRE, DENIS','011/588.443',3,0,1466514292,'Voudrait une offre rapide pour vendre tout de suite soi-disant.\r\nPÃ¨re de 96 ans veut vendre d\'urgence, il n\'a rien d\'autre.',0);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-23 11:18:41
